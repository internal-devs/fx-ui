# Configuration
### Docs
- [Official Page](https://openjfx.io/)
- [Download Page](https://gluonhq.com/products/javafx/)
- [IDES Configuration](https://openjfx.io/openjfx-docs/)
- [Java Docs](https://openjfx.io/javadoc/21/)
- [Examples](https://fxdocs.github.io/docs/html5/)
- [Blog](https://www.baeldung.com/)

### Add VM Options
```bash
  --module-path "...\javafx-sdk-21.0.2\lib" --add-modules javafx.controls,javafx.fxml
```
