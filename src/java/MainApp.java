import com.stark.app.view.LoginView;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainApp extends Application {

    @Override
    public void start(Stage stage) {
        LoginView loginView = new LoginView();
        loginView.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
