package com.stark.app.http;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HttpClientUtil {

    public HttpResponse<String> login(String username, String password) throws IOException, InterruptedException {
        URI uri = URI.create("http://localhost:8090/api/v1/auth");

        String body = String.format("""
                {
                    "username": "%s",
                    "password": "%s"
                }
                """, username, password);

        try (HttpClient httpClient = HttpClient.newBuilder().build()) {
            HttpRequest.BodyPublisher bodyPublisher = HttpRequest.BodyPublishers.ofString(body);

            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(uri)
                    .header("Content-Type", "application/json")
                    .POST(bodyPublisher)
                    .build();
            return httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        }
    }
}
