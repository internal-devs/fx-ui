package com.stark.app.view;

import com.stark.app.http.HttpClientUtil;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import java.net.http.HttpResponse;

public class LoginView extends Stage {

    public LoginView() {
        setResizable(false);
        setTitle("Login");

        VBox vBox = new VBox(30.0);
        //config
        vBox.setPadding(new Insets(20.0));
        vBox.setAlignment(Pos.CENTER);

        Font font = new Font("Serif", 20.0);
        Label lblTitle = new Label("Login Stark");
        lblTitle.setFont(font);

        TextField txtUsername = new TextField();
        txtUsername.setPromptText("Username");
        txtUsername.setFont(font);

        PasswordField txtPassword = new PasswordField();
        txtPassword.setPromptText("Password");
        txtPassword.setFont(font);

        Button btnLogin = new Button("Login");
        btnLogin.setFont(font);

        btnLogin.setOnAction(actionEvent -> {
            String username = txtUsername.getText();
            String password = txtPassword.getText();

            if(username.trim().isEmpty() || password.trim().isEmpty()) {
                showAlert("System error", "Username or password incorrect", Alert.AlertType.WARNING);
            } else {
                //Login Api call
                try {
                    HttpClientUtil httpClientUtil = new HttpClientUtil();
                    HttpResponse<String> response = httpClientUtil.login(username, password);

                    if(response.statusCode() == 200) {
                        showAlert("Login OK", "Login Successful", Alert.AlertType.INFORMATION);
                        close();
                    } else {
                        showAlert("System error", "Username or password incorrect", Alert.AlertType.WARNING);
                    }
                } catch (Exception e) {
                    showAlert("System error", "Internal Server Error", Alert.AlertType.ERROR);
                }
            }
        });

        vBox.getChildren().addAll(
                lblTitle,
                txtUsername,
                txtPassword,
                btnLogin
        );

        Scene scene = new Scene(vBox, 300, 400);
        setScene(scene);
    }

    private void showAlert(String title, String content, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setContentText(content);
        alert.show();
    }
}
